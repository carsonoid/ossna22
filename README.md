# ossna22

## Getting started

Welcome to the GitLab Open Source Summit Code Challenge hosted on CodeChallenge.dev. We're so glad you're here.

## Level 1

Simply fork this project into your own namespace to get started.

Once that is done, fix the spelling errors and other typos in this `README.md`. Then make a [pull request](https://gitlab.com/gitlab-code-challenge/ossna22/-/merge_requests) to this repository.

## Level 2

For level two, you can use the [Static Application Security Testing (SAST) Documentation](https://docs.gitlab.com/ee/user/application_security/sast/) to find out how to configure SAST manually and add it to this project.

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to: The Ruby on Rails backend, the Vue-based frontend, the Go-based services like the GitLab Runner and Gitaly, and the documentation for all of those things and more.

